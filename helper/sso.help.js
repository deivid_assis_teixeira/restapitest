const request = require('request')

const getUserAgent = () => {
    return 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.87 Safari/537.36'
}

const getToken = () => {
    return new Promise((resolve, rejetc) => {

        const options = {
            method: 'GET',
            url: 'https://contabilizeiapp-dev-01.appspot.com/public/requestLogin',
            followRedirect: false,
            qs: { redirect: '/' },
            headers:
            {
                'User-Agent': getUserAgent()
            }
        }

        request(options, (error, response) => {
            if (error) rejetc(error);

            const token = response.headers.location.split('token=')[1]

            resolve(token);
        });

    })
}

const getOuth = token => {
    return new Promise((resolve, reject) => {
        const options = {
            method: 'POST',
            url: 'https://contabilizei-sso-teste.appspot.com/login',
            followRedirect: false,
            headers:
            {
                'User-Agent': getUserAgent()
            },
            form: {
                user: 'admin@contabilizei.com.br',
                password: 'iddqd',
                token: token
            }
        }

        request(options, (error, response) => {
            if (error) reject(error)

            const outh = response.headers["set-cookie"][0]

            resolve(outh)
        })
    })
}

const getCookiesAdmin = cookie => {

    const oathCookie = cookie
    const outhCookieAdmin = cookie.replace('oauth-token', 'oauth-token-admin')
    const cookieJar = request.jar()

    cookieJar.setCookie(oathCookie, 'https://contabilizeiapp-dev-01.appspot.com')
    cookieJar.setCookie(outhCookieAdmin, 'https://contabilizeiapp-dev-01.appspot.com')
    return cookieJar
}

const getCookies = async () => {
    const tokenLoginRequest = await getToken()
    const cookie = await getOuth(tokenLoginRequest)

    return getCookiesAdmin(cookie)
}

module.exports = SsoHelper = {
    getCookies: getCookies,
    getUserAgent: getUserAgent
}