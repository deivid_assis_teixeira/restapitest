const request = require('request')
const chai = require('chai')
chai.use(require('chai-as-promised'))
const assert = chai.assert
const should = chai.should()
const SSO_Helper = require('../helper/sso.help')

const baseUrl = 'https://contabilizeiapp-dev-01.appspot.com/cron/callbackAuditoriaNFSe'

let options

before(async function () {
    this.timeout(30000)
    options = {
        followRedirect: false,
        headers: {
            'User-Agent': SSO_Helper.getUserAgent()
        },
        jar: await SSO_Helper.getCookies()
    }
})

describe('Verifica resposta 200', () => {

    it('Será...?', async function () {

        this.timeout(5000)

        options.method = 'GET'
        options.url = baseUrl

        const executaRequest = () => {
            return new Promise((resolve, reject) => {
                request(options, (err, response) => {
                    if (err) reject(err)
                    resolve(response.statusCode)
                })
            })
        }

        const statusCode = await executaRequest();

        statusCode.should.equal(200);

    })
})